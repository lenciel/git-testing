#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseForbidden
from django.utils import simplejson
from django.core import serializers
from django.core.urlresolvers import reverse
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import permission_required, login_required
from django.db import IntegrityError
from django.contrib import auth
from django.contrib.auth.models import User, Group, Permission
from django.conf import settings
from guardian.shortcuts import assign, remove_perm
from decorators import ajax_login_required, ajax_permission_required, pre_condition
from ajaxrequest import HttpResponseJson, JsonInsertRequest, JsonDeleteRequest, JsonUpdateRequest
import logging
import dbutil
from response_codes import get_response_code
from .models import Province, City, District, Hall, UserInfo, Download, App
from .forms import NewAppForm, EditAppForm
from account.apis import can_access_special_apps
from account.views import INVALID_PROVINCES, _filter_province
from django.db.models import Q
from datetime import datetime
from time import time
from log import ColorFormatter

REGION_TYPE = ['hall', 'district', 'city', 'province']
REGION_MODELS = {
    'hall': Hall,
    'district': District,
    'city': City,
    'province': Province,
    }
# keys
# user data
KEY_ACCOUNT_OPEN_NUM = u'账号开通数'
KEY_ACCOUNT_ACTIVE_NUM = u'账号活跃数'
KEY_ACCOUNT_ACTIVE_RATE = u'账号活跃率'
KEY_HALL_OPEN_NUM = u'厅店开通数'
KEY_HALL_ACTIVE_NUM = u'厅店活跃数'
KEY_HALL_ACTIVE_RATE = u'厅店活跃率'
KEY_ASSISTANT_NUM = u'厅店营业员数量'
USER_DATA_KEYS = [KEY_ACCOUNT_OPEN_NUM, KEY_ACCOUNT_ACTIVE_NUM,
                  KEY_ACCOUNT_ACTIVE_RATE, KEY_HALL_OPEN_NUM,
                  KEY_HALL_ACTIVE_NUM, KEY_HALL_ACTIVE_RATE, KEY_ASSISTANT_NUM]

# app scope
KEY_IS_ALL_APP = 'isAllApp'

# channel
KEY_CHANNEL_CELL = u'手机版'
KEY_CHANNEL_HALL = u'厅店版'
KEY_CHANNEL_HALL_PUSH = u'厅店版push'
KEY_CHANNEL_WAREHOUSE = u'仓库版'
CHANEL_KEYS = [KEY_CHANNEL_WAREHOUSE, KEY_CHANNEL_HALL,
               KEY_CHANNEL_CELL, KEY_CHANNEL_HALL_PUSH]

# transaction
KEY_TRANS_USER_NUM = u'新增用户数'
KEY_TRANS_DOWNLOAD_USER_NUM = u'下载用户数'
KEY_TRANS_DOWNLOAD_APP_NUM = u'应用下载量'
KEY_TRANS_FLOW = u'流量'
KEY_TRANS_FLOW_COACH = u'辅导流量'
KEY_TRANS_FLOW_COACH_AVG = u'辅导户均流量'
KEY_TRANS_FLOW_MEAN_DIFF = u'流量均值差'
KEY_TRANS_FLOW_MEAN_RATIO = u'流量均值比'
TRANS_KEYS = [
    KEY_TRANS_USER_NUM, KEY_TRANS_DOWNLOAD_USER_NUM, KEY_TRANS_DOWNLOAD_APP_NUM,
    KEY_TRANS_FLOW, KEY_TRANS_FLOW_COACH, KEY_TRANS_FLOW_COACH_AVG,
    KEY_TRANS_FLOW_MEAN_DIFF, KEY_TRANS_FLOW_MEAN_RATIO,
]

# download user num report
KEY_REPORT_HALL_ACTIVE_NUM_DAILY = u'厅店日均活跃度'
KEY_REPORT_USER_ACTIVE_NUM_DAILY = u'帐号日均活跃度'
KEY_REPORT_DOWNLOAD_USER_NUM_HALL_CURRENT = u'当期厅店版下载用户数'
KEY_REPORT_DOWNLOAD_USER_NUM_CELL_CURRENT = u'当期手机版下载用户数'
KEY_REPORT_DOWNLOAD_USER_NUM_CURRENT = u'当期下载用户总数'
KEY_REPORT_DOWNLOAD_USER_NUM_HALL_TOTAL = u'厅店版下载用户数全年累计'
KEY_REPORT_DOWNLOAD_USER_NUM_CELL_TOTAL = u'手机版下载用户数全年累计'
KEY_REPORT_DOWNLOAD_USER_NUM_TOTAL = u'全年累计下载用户总数'
DOWNLOAD_USER_REPORT_KEYS = [
    KEY_REPORT_HALL_ACTIVE_NUM_DAILY,
    KEY_REPORT_USER_ACTIVE_NUM_DAILY,
    KEY_REPORT_DOWNLOAD_USER_NUM_HALL_CURRENT,
    KEY_REPORT_DOWNLOAD_USER_NUM_CELL_CURRENT,
    KEY_REPORT_DOWNLOAD_USER_NUM_CURRENT,
    KEY_REPORT_DOWNLOAD_USER_NUM_HALL_TOTAL,
    KEY_REPORT_DOWNLOAD_USER_NUM_CELL_TOTAL,
    KEY_REPORT_DOWNLOAD_USER_NUM_TOTAL,
]

# 下载量报表keys
KEY_REPORT_APP_DOWNLOAD_NUM_CURRENT = u'当期下载量'
KEY_REPORT_APP_DOWNLOAD_NUM_TOTAL = u'下载量全年累计'

KEY_REPORT_APP_DOWNLOAD_NUM_HALL_CURRENT = u'当期厅店版下载量'
KEY_REPORT_APP_DOWNLOAD_NUM_CELL_CURRENT = u'当期手机版下载量'
KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_CURRENT = u'当期仓库版下载量'
KEY_REPORT_APP_DOWNLOAD_NUM_ALL_CURRENT = u'当期下载总量'
KEY_REPORT_APP_DOWNLOAD_NUM_HALL_YEAR = u'厅店版下载量全年累计'
KEY_REPORT_APP_DOWNLOAD_NUM_CELL_YEAR = u'手机版下载量全年累计'
KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_YEAR = u'仓库版下载量全年累计'
KEY_REPORT_APP_DOWNLOAD_NUM_ALL_YEAR = u'全年累计下载总数'

DOWNLOAD_APP_REPORT_KEYS = [
    # 部分应用
    KEY_REPORT_APP_DOWNLOAD_NUM_CURRENT,
    KEY_REPORT_APP_DOWNLOAD_NUM_TOTAL,
    # 全部应用
    KEY_REPORT_HALL_ACTIVE_NUM_DAILY,
    KEY_REPORT_USER_ACTIVE_NUM_DAILY,
    KEY_REPORT_APP_DOWNLOAD_NUM_HALL_CURRENT,
    KEY_REPORT_APP_DOWNLOAD_NUM_CELL_CURRENT,
    KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_CURRENT,
    KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_YEAR,
    KEY_REPORT_APP_DOWNLOAD_NUM_HALL_YEAR,
    KEY_REPORT_APP_DOWNLOAD_NUM_ALL_YEAR,
]

# 下载流量报表keys
KEY_REPORT_TRAFFIC_CELL_CURRENT = u'当期手机版产生流量'
KEY_REPORT_TRAFFIC_HALL_PUSH_CURRENT = u'当期厅店PUSH产生流量'
KEY_REPORT_TRAFFIC_HALL_CURRENT = u'当期厅店版产生流量'
KEY_REPORT_TRAFFIC_ALL_CURRENT = u'当期产生流量总计'
KEY_REPORT_TRAFFIC_ALL_AVG_CURRENT = u'当期户均流量'
KEY_REPORT_TRAFFIC_CELL_YEAR = u'手机版产生流量全年累计'
KEY_REPORT_TRAFFIC_HALL_PUSH_YEAR = u'厅店PUSH产生流量全年累计'
KEY_REPORT_TRAFFIC_HALL_YEAR = u'厅店版产生流量全年累计'
KEY_REPORT_TRAFFIC_ALL_YEAR = u'全年累计产生流量'
KEY_REPORT_TRAFFIC_ALL_AVG_YEAR = u'全年累计户均流量'

DOWNLOAD_TRAFFIC_REPORT_KEYS = [
    KEY_REPORT_TRAFFIC_CELL_CURRENT,
    KEY_REPORT_TRAFFIC_HALL_PUSH_CURRENT,
    KEY_REPORT_TRAFFIC_HALL_CURRENT,
    KEY_REPORT_TRAFFIC_ALL_CURRENT,
    KEY_REPORT_TRAFFIC_ALL_AVG_CURRENT,
    KEY_REPORT_TRAFFIC_CELL_YEAR,
    KEY_REPORT_TRAFFIC_HALL_PUSH_YEAR,
    KEY_REPORT_TRAFFIC_HALL_YEAR,
    KEY_REPORT_TRAFFIC_ALL_YEAR,
    KEY_REPORT_TRAFFIC_ALL_AVG_YEAR,
]

# 下载流量比报表
KEY_REPORT_RATIO_USER_3G_AVG = '助手辅导用户3G户均流量'
KEY_REPORT_RATIO_USER_3G_TERMINAL_AVG = 'ratioUser3GTerminalAvg'
KEY_REPORT_RATIO_DIFF = 'ratioDiff'
KEY_REPORT_RATIO_MEAN_DIFF = 'ratioMeanDiff'

DOWNLOAD_RATIO_KEYS = [
    KEY_REPORT_RATIO_USER_3G_AVG,
    KEY_REPORT_RATIO_USER_3G_TERMINAL_AVG,
    KEY_REPORT_RATIO_DIFF,
    KEY_REPORT_RATIO_MEAN_DIFF,
]

TIME_FORMAT = '%Y-%m-%d'
START_OF_YEAR = '%s-01-01' % datetime.now().year

logger = logging.getLogger('apps.report')
file_handler = logging.FileHandler('report.log')
file_handler.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(levelname)s %(asctime)s %(filename)s %(lineno)d %(funcName)s: %(message)s")
formatter = ColorFormatter()
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

def log_time(report_function):
    """
    记录函数执行时间
    """
    def wrap(*args, **kwargs):
        now = time()
        try:
            return report_function(*args, **kwargs)
        finally:
            if args:
                region = args[0]
                key = args[-1]
            if kwargs:
                region = kwargs['region'].name
                key = kwargs['key']
            logger.info("args(%s, %s) tm=%s" %
                        (
                            region,
                            key,
                            float_format(time() - now)
                        )
            )
    wrap.__doc__ = report_function.__doc__
    wrap.__dict__ = report_function.__dict__
    return wrap


@login_required
def report_home_view(request):
    context = {'title': '报表查询'}
    return render_to_response('report/home.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def dashboard_view(request):
    return render_to_response('report/dashboard.inc.html',
                              locals(),
                              context_instance=RequestContext(request))


def get_userdefined_apps():
    """
    返回自定义的应用列表，供报表统计使用
    """
    return App.objects.filter(is_userdefined=True)


def app_view(request):
    """
    显示自定义的App列表。
    """

    _enforce_app_perm_check(request)

    apps = get_userdefined_apps()
    return render_to_response('report/app.inc.html',
                              locals(),
                              context_instance=RequestContext(request))


def app_create_view(request):
    """
    录入一个新的App，如果该App对应的ID已经存在，则更新之i。
    """

    _enforce_app_perm_check(request)

    if request.method == 'GET':
        form_action = reverse('report.views.app_create_view')
        form = NewAppForm(auto_id=True)
        return render_to_response('report/app.new.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        return JsonInsertRequest(request, NewAppForm).process()
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def app_edit_view(request, app_id):
    """
    修改已有的App信息，当前只能修改App的名字。
    """

    _enforce_app_perm_check(request)

    if request.method == 'GET':
        form_action = reverse('report.views.app_edit_view',
                              kwargs={'app_id': app_id})
        app = get_object_or_404(App, id=app_id)
        form = EditAppForm(instance=app)
        return render_to_response('report/app.edit.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        return JsonUpdateRequest(request, App, EditAppForm, app_id).process()
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def app_delete_view(request, app_id):
    """
    删除改自定义App，在数据库里只是把is_userdefined字段标识为False。
    """

    _enforce_app_perm_check(request)

    app = get_object_or_404(App, id=app_id)

    app.is_userdefined = False
    app.save();

    result = get_response_code('success')

    return HttpResponseJson(result)


def _enforce_app_perm_check(request):
    """
    检查当前用户是否有权限访问App管理的功能。如果没有权限这返回HTTP 403错误。

    只有超级用户和全国用户才有访问App管理的功能。
    """
    if not can_access_special_apps(request.user):
        raise PermissionDenied


def user_data_customized_view(request):
    """
    自定义账号指标报表

    根据查询限制条件:
    1. 时间
    2. 地域
    生成指定报表:

    厅店开通数
        规则： 状态为启用的厅店总数，参考 hall_info 中的 s 字段

    厅店活跃数
        规则： 活动期间有过下载的厅店总数

    厅店活跃率
        规则： 厅店活跃数/厅店开通数

    厅店版营业员数量
        规则： 指定厅店下的状态为”启用”的账号总数，参考 user_info 的 s 字段

    账号开通数
        规则： 状态为”启用”的操作员账号总数，参考 user_info 的 s 和 ul 字段

    账号活跃数
        规则： 活动期间产生过下载的账号总数

    账号活跃率
        规则： 账号活跃数/账号开通数
    """
    if request.method == 'GET':
        form_action = reverse('report.views.user_data_customized_view')
        return render_to_response('report/user.data.customize.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))

    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}

            min_region = _get_min_region(conditions)
            time_duration = [conditions['startDate'], conditions['endDate']]

            # 厅店开通数
            # 简单检查该region下hall.status为1的就行
            _get_hall_open_num(min_region, time_duration, result,
                               conditions, key=KEY_HALL_OPEN_NUM)

            # 厅店活跃数
            # 有过下载的厅
            _get_hall_active_num(min_region, time_duration, result,
                                 conditions,  key=KEY_HALL_ACTIVE_NUM)

            # 厅店活跃率
            # 活越数/总数
            _get_hall_active_rate(min_region, time_duration, result,
                                  conditions, key=KEY_HALL_ACTIVE_RATE)

            # 厅店版营业员数量
            _get_assistant_num(min_region, time_duration, result,
                               conditions, key=KEY_ASSISTANT_NUM)

            # 帐号开通数
            _get_account_open_num(min_region, time_duration, result,
                                  conditions, key=KEY_ACCOUNT_OPEN_NUM)

            # 帐号活跃数
            _get_account_active_num(min_region, time_duration, result,
                                    conditions, key=KEY_ACCOUNT_ACTIVE_NUM)

            # 帐号活跃率
            _get_account_active_rate(min_region, time_duration, result,
                                     conditions, key=KEY_ACCOUNT_ACTIVE_RATE)

            response['table_data'] = result

        return HttpResponseJson(response)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


@pre_condition
@log_time
def _get_downloadinfo_flow_mean_ratio(min_region, time_duration, result, conditions, key):
    #TODO
    region = _get_region_model(min_region)
    res = dict()
    for sub_region in _get_sub(region):
        res[sub_region.name] = 0
    result[key] = res


@pre_condition
@log_time
def _get_downloadinfo_flow_mean_diff(min_region, time_duration, result, conditions, key):
    """
    3G户均流量均值差
    规则： 均值差 = 助手辅导用户3G户均流量 - 智能终端用户3G户均流量
    """
    #TODO
    region = _get_region_model(min_region)
    res = dict()
    for sub_region in _get_sub(region):
        res[sub_region.name] = 0
    result[key] = res
    pass


def _get_downloadinfo_flow_terminal(min_region, time_duration, result, conditions, key):
    """
    智能终端用户3G户均流量
    规则： 通过excel导入。 excel数据分两列，一列是省名称，一列是流量数据。
    """
    #TODO
    result[key] = {}
    pass


# TODO 页面还没有该选项，暂时不会触发
@pre_condition
@log_time
def _get_downloadinfo_flow_coach_avg(min_region, time_duration, result, conditions, key):
    conditions[KEY_TRANS_FLOW_COACH] = True
    _get_downloadinfo_flow_coach(min_region, time_duration, result,
                                 conditions, KEY_TRANS_FLOW_COACH)
    region = _get_region_model(min_region)
    res = {}
    sub_type = _get_sub_type(region)
    for sub_region in _get_sub(region):
        user_count = UserInfo.objects.filter(**{sub_type: sub_region}).count()
        try:
            res[sub_region.name] = float(result[KEY_TRANS_FLOW_COACH][sub_region.name]) / user_count
        except ZeroDivisionError:
            res[sub_region.name] = 'NAN'
    result[key] = res


@pre_condition
@log_time
def _get_downloadinfo_flow_coach(min_region, time_duration, result, conditions, key):
    """
    辅导用户总流量
    """
    region = _get_region_model(min_region)
    downloads = _get_download_by_region(region, time_duration, conditions,
                                        Q(source=2) | Q(source=3) | Q(source=4))

    total = 0
    if min_region['type'] == 'hall':
        for d in downloads:
            total += d.download_size
        result[key] = {region.name: total}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            sub_downloads = downloads.filter(**{sub_type: sub_region})
            sub_total = 0
            for d in sub_downloads:
                sub_total += d.download_size
            res = dict(res, **{sub_region.name: sub_total})
            total += sub_total
        result[key] = res
    #logger.info('result[%s]: %s' % (key, str(result[key])))


@pre_condition
@log_time
def _get_downloadinfo_flow(min_region, time_duration, result, conditions, key):
    """
    范围： 各版本指厅店版、手机版、厅店版Push，总数指这些版本的总和，参考 download_info 的 t 字段
    规则： 下载流量 = 下载次数*应用大小（参考 download_info 的 az 字段）
    """
    region = _get_region_model(min_region)
    q = _get_channel_app_type_q(conditions)
    downloads = _get_download_by_region(region, time_duration, conditions, q)

    total = 0
    if min_region['type'] == 'hall':
        for d in downloads:
            total += d.download_size
        result[key] = {region.name: total}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            sub_downloads = downloads.filter(**{sub_type: sub_region})
            sub_total = 0
            for d in sub_downloads:
                sub_total += d.download_size
            res = dict(res, **{sub_region.name: sub_total})
            total += sub_total
        result[key] = res
    #logger.info('result[%s]: %s' % (key, str(result[key])))


@pre_condition
@log_time
def _get_downloadinfo_app_num(min_region, time_duration, result, conditions, key):
    """
    范围： 各版本指厅店版、手机版、厅店版Push、仓库版，总数指这些版本的总和，参考 download_info 的 t 字段
    规则： 下载量为产生的下载次数，不需要进行剔重
    """
    region = _get_region_model(min_region)
    q = _get_channel_app_type_q(conditions)
    downloads = _get_download_by_region(region, time_duration, conditions, q)
    total = 0
    if min_region['type'] == 'hall':
        total = downloads.count()
        result[key] = {region.name: total}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            sub_downloads = downloads.filter(**{sub_type: sub_region})
            sub_total = sub_downloads.count()
            res = dict(res, **{sub_region.name: sub_total})
            total += sub_total
        result[key] = res
    #logger.info('result[%s]: %s' % (key, str(result[key])))


@pre_condition
@log_time
def _get_user_num_newly_incr(min_region, time_duration, result, conditions, key):
    """
    新增消费者数
    """
    conditions[key] = True
    _get_downloadinfo_user_num(min_region, [START_OF_YEAR, time_duration[1]], result, conditions, key)
    #logger.info('result[%s]: %s' % (key, str(result[key])))


def _get_channel_app_type_q(conditions):
    ret = None
    for key in CHANEL_KEYS:
        if conditions.get(key):
            if ret:
                ret = ret | Q(source=CHANEL_KEYS.index(key))
            else:
                ret = Q(source=CHANEL_KEYS.index(key))

    return ret


@pre_condition
@log_time
def _get_downloadinfo_user_num(min_region, time_duration, result, conditions, key):
    """
    范围： 各版本指厅店版、手机版、厅店版Push、仓库版，总数指这些版本的总和，参考 download_info 的 t 字段
    规则： 在各个版本产生过下载的消费者数，需要按照IMEI进行剔重，参考 download_info 中的相应字段
    """
    region = _get_region_model(min_region)
    q = _get_channel_app_type_q(conditions)
    downloads = _get_download_by_region(region, time_duration, conditions, q)
    total = 0
    if min_region['type'] == 'hall':
        users = []
        for d in downloads:
            users.append(d.referrer.imei)
        total = len(sorted(set(users)))
        result[key] = {region.name: total}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            users = []
            # sub_downloads = downloads.filter(**{sub_type: sub_region})
            sub_downloads = _my_downloads_filter(downloads, sub_type, sub_region)
            for d in sub_downloads:
                users.append(d.imei)
            sub_total = len(sorted(set(users)))
            res = dict(res, **{sub_region.name: sub_total})
            total += sub_total
        result[key] = res
    # logger.info('result[%s]: %s' % (key, str(result[key])))


# FIXME 某些region调用非常慢，例如成都->成都市辖区
def _my_user_filter(users, region_type, region):
    start = time()
    print len(users), region.name, region_type
    # a = [u for u in users if getattr(u, region_type) == region]
    # a = users.filter(**{region_type: region})
    a = UserInfo.objects.filter()
    print time() - start
    print a
    return a

def _my_downloads_filter(downloads, sub_type, sub_region):
    return [d for d in downloads if getattr(d, sub_type) == sub_region]


@pre_condition
@log_time
def _get_account_active_rate(min_region, time_duration, result, conditions, key):
    # 账号活跃数/账号开通数
    conditions[KEY_ACCOUNT_ACTIVE_NUM] = True
    conditions[KEY_ACCOUNT_OPEN_NUM] = True
    _get_account_active_num(min_region, time_duration, result, conditions, KEY_ACCOUNT_ACTIVE_NUM)
    _get_account_open_num(min_region, time_duration, result, conditions, KEY_ACCOUNT_OPEN_NUM)
    region = _get_region_model(min_region)
    if min_region['type'] == 'hall':
        try:
            result[key] = {region.name: float_format(float(result[KEY_ACCOUNT_ACTIVE_NUM][region.name]) / float(result[KEY_ACCOUNT_OPEN_NUM][region.name])) }
        except ZeroDivisionError:
            result[key] = {region.name: 'NAN'}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            try:
                res = dict(res, **{
                    sub_region.name: float_format( float(result[KEY_ACCOUNT_ACTIVE_NUM][sub_region.name])
                                                   / float(result[KEY_ACCOUNT_OPEN_NUM][sub_region.name])
                                                )
                    })
            except ZeroDivisionError:
                res = dict(res, **{sub_region.name: 'NAN'})
        result[key] = res

@pre_condition
@log_time
def _get_account_active_num(min_region, time_duration, result, conditions, key):
    region = _get_region_model(min_region)
    downloads = _get_download_by_region(region, time_duration, conditions)

    if min_region['type'] == 'hall':
        users = []
        for d in downloads:
            users.append(d.referrer.id)
        result[key] = {region.name: len(sorted(set(users)))}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            users = []
            sub_downloads = _my_downloads_filter(downloads, sub_type, sub_region)
            for d in sub_downloads:
                users.append(d.referrer.id)
            res = dict(res, **{sub_region.name: len(sorted(set(users)))})
        result[key] = res


@pre_condition
@log_time
def _get_account_open_num(min_region, time_duration, result, conditions, key):
    """
    userinfo中,grade=1, status=1
    """
    region = _get_region_model(min_region)
    filter_args = {"grade": True, 'status': True}

    if min_region['type'] == 'hall':
        filter_args[min_region['type']] = region
        account_num = len(UserInfo.objects.filter(**filter_args))
        result[key] = {region.name: account_num}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            # account_num = len(_my_user_filter(all_users, sub_type, sub_region))
            filter_args[sub_type] = sub_region
            account_num = UserInfo.objects.filter(**filter_args).count()
            res = dict(res, **{sub_region.name: account_num})
        result[key] = res


@pre_condition
@log_time
def _get_assistant_num(min_region, time_duration, result, conditions, key):
    """
    userinfo中，grade=1
    营业员数量
    """
    # 获取营业厅总数
    region = _get_region_model(min_region)
    if min_region['type'] == 'hall':
        filter_args = {min_region['type']: region, "grade": True}
        user_num = UserInfo.objects.filter(**filter_args).count()
        result[key] = {region.name: user_num}
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            filter_args = {sub_type: sub_region, "grade": True}
            user_num = UserInfo.objects.filter(**filter_args).count()
            res = dict(res, **{sub_region.name: user_num})
        result[key] = res


def float_format(f):
    """
    保留两位小数
    """
    return round(f, 2)


@pre_condition
@log_time
def _get_hall_active_rate(min_region, time_duration, result, conditions, key):
    # 获取活跃数
    conditions[KEY_HALL_ACTIVE_NUM] = True
    _get_hall_active_num(min_region, time_duration, result, conditions, KEY_HALL_ACTIVE_NUM)
    # 获取总数
    conditions[KEY_HALL_OPEN_NUM] = True
    _get_hall_open_num(min_region, time_duration, result, conditions, KEY_HALL_OPEN_NUM)

    region = _get_region_model(min_region)
    if min_region['type'] == 'hall':
        try:
            result[key] = {region.name: float_format(float(result[KEY_HALL_ACTIVE_NUM]) / float(result[KEY_HALL_OPEN_NUM]))}
        except ZeroDivisionError:
            result[key] = {region.name: 'NAN'}
    else:
        res = {}
        for sub_region in _get_sub(region):
            hall_active_num = float(result[KEY_HALL_ACTIVE_NUM][sub_region.name])
            hall_open_num = float(result[KEY_HALL_OPEN_NUM][sub_region.name])
            try:
                res = dict(res, **{sub_region.name: float_format(hall_active_num/hall_open_num) })
            except ZeroDivisionError:
                res = dict(res, **{sub_region.name: 'NAN'})
        result[key] = res


def _single_hall_active_num(downloads, region, time_duration, conditions):
    res = dict()
    for d in downloads:
        if not res.get(d.hall.id):
            res[d.hall.id] = 1
    return {region.name: len(res)}


@pre_condition
@log_time
def _get_hall_active_num(min_region, time_duration, result, conditions, key):
    """
    filter download，条件为id=最小一级region
    """
    region = _get_region_model(min_region)
    downloads = _get_download_by_region(region, time_duration, conditions)
    if min_region['type'] == 'hall':
        result[key] = _single_hall_active_num(downloads, region, time_duration, conditions)
    else:
        res = {}
        sub_type = _get_sub_type(region)
        for sub_region in _get_sub(region):
            sub_downloads = _my_downloads_filter(downloads, sub_type, sub_region)
            res = dict(res, **_single_hall_active_num(sub_downloads, sub_region, time_duration, conditions))
        result[key] = res


def _single_hall_open_num(region):
    halls = _get_hall_by_region(region)
    return {region.name: len(halls)}


@pre_condition
@log_time
def _get_hall_open_num(min_region, time_duration, result, conditions, key):
    region = _get_region_model(min_region)
    if min_region['type'] == 'hall':
        # 叶子节点
        result[key] = _single_hall_open_num(region)
    else:
        # 显示下级节点列表
        res = {}
        for sub_region in _get_sub(region):
            res = dict(res, **_single_hall_open_num(sub_region))
        result[key] = res


def _get_min_region(regions):
    for t in REGION_TYPE:
        if regions[t] != '':
            return {'type': t, 'id': int(regions[t])}
    return {'', -1}


def _get_region_model(region_info):
    return REGION_MODELS[region_info['type']].objects.get(**{'id': region_info['id']})


def _get_type(region):
    return region.__class__.__name__.lower()


def _get_sub_type(region):
    region_type = _get_type(region)
    index = REGION_TYPE.index(region_type)
    if index == 0:
        return ''
    else:
        return REGION_TYPE[index - 1]

# TODO 避免重复查询
def _get_sub(region):
    """
    get children of region
    """
    region_type = _get_type(region)
    if region_type == 'hall':
        return []
    else:
        return REGION_MODELS[_get_sub_type(region)].objects.filter(**{region_type: region})


def _get_hall_by_region(region):
    region_type = region.__class__.__name__.lower()
    if region_type == 'hall':
        return [region]
    return Hall.objects.filter(**{region_type: region, "status": True})

#TODO cache
def _get_download_by_region(region, time_duration, conditions, q=None):
    region_type = _get_type(region)
    default_filter = {region_type: region, 'download_date__range': time_duration, }

    start_time = time()
    if not time_duration:
        default_filter.pop('download_date__range')
    if conditions.has_key(KEY_IS_ALL_APP):
        default_filter['app__is_userdefined'] = conditions[KEY_IS_ALL_APP]
    if q:
        logger.info(u"get download with Q from %s: tm=%s " % (region.name, float_format(time() - start_time)))
        return Download.objects.select_related().filter(q, **default_filter)
    else:
        logger.info(u"get download from %s: tm=%s " % (region.name, float_format(time() - start_time)))
        return Download.objects.select_related().filter(**default_filter)


def download_info_customized_view(request):
    """
    自定义账号指标报表
    """
    if request.method == 'GET':
        form_action = reverse('report.views.download_info_customized_view')
        return render_to_response('report/download.info.customize.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}

            min_region = _get_min_region(conditions)
            time_duration = [conditions['startDate'], conditions['endDate']]

            arguments = [min_region, time_duration, result, conditions]

            _get_user_num_newly_incr(*arguments + [KEY_TRANS_USER_NUM])

            _get_downloadinfo_user_num(*arguments + [KEY_TRANS_DOWNLOAD_USER_NUM])

            _get_downloadinfo_app_num(*arguments + [KEY_TRANS_DOWNLOAD_APP_NUM])

            _get_downloadinfo_flow(*arguments + [KEY_TRANS_FLOW])

            _get_downloadinfo_flow_coach(*arguments + [KEY_TRANS_FLOW_COACH])

            # TODO
            # 助手辅导用户3G户均流量
            # 户均流量 = 辅导用户总流量/[厅店版+厅店版Push+手机Push]完成下载用户数
            _get_downloadinfo_flow_coach_avg(*arguments + [KEY_TRANS_FLOW_COACH_AVG])

            # TODO
            # 智能终端用户3G户均流量
            # 规则： 通过excel导入。 excel数据分两列，一列是省名称，一列是流量数据。

            # 3G户均流量均值差¶
            # 规则： 均值差 = 助手辅导用户3G户均流量 - 智能终端用户3G户均流量
            _get_downloadinfo_flow_mean_diff(*arguments + [KEY_TRANS_FLOW_MEAN_DIFF])
            # TODO
            # 3G户均流量均值比
            # 规则： 均值比 = 助手辅导用户3G户均流量 / 智能终端用户3G户均流量
            _get_downloadinfo_flow_mean_ratio(*arguments + [KEY_TRANS_FLOW_MEAN_RATIO])
            response['table_data'] = result

        return HttpResponseJson(response)

    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def child_area_view_json(request):
    """
    level对应关系
    1   全国
    2   省
    3   市
    4   区
    5   营业厅
    """
    id = int(request.GET.get('id'))
    level = int(request.GET.get('level'))
    page_type = request.GET.get('page_type')
    result = {}

    if level == 0:
        result = {"1": "全国"}
    elif level == 1:
        provinces = _filter_province(Province.objects.all())
        for province in provinces:
            result[province.id]=province.name
    elif level == 2:
        cities = City.objects.filter(province=Province.objects.get(id=id))
        for city in cities:
            result[city.id] = city.name
    elif level == 3:
        districts = District.objects.filter(city=City.objects.get(id=id))
        for district in districts:
            result[district.id] = district.name
    elif level == 4:
        halls = Hall.objects.filter(district=District.objects.get(id=id))
        for hall in halls:
            result[hall.id] = hall.name
    return HttpResponseJson(result)


def customer_download_count_view(request):
    """
    渠道业务量统计报表->下载用户数报表

    全部应用：
    1. 报表时间
    2. 区域名称
    3. 厅店日均活跃度
    4. 帐号日均活跃度
    5. 当期厅店版下载用户数
    6. 当期手机版下载用户数
    7. 当期下载用户总数
    8. 厅店版下载用户数全年累计
    9. 手机版下载用户数全年累计
    10. 全年累计下载用户总数

    指定应用：
    1. 报表时间
    2. 区域名称
    3. 当期下载用户数
    4. 下载用户数全年累计
    """
    if request.method == 'GET':
        form_action = reverse('report.views.customer_download_count_view')
        return render_to_response('report/customer.download.count.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))

    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}

            min_region = _get_min_region(conditions)
            time_duration = [conditions['startDate'], conditions['endDate']]
            time_duration_year = [START_OF_YEAR, conditions['endDate']]

            if conditions[KEY_IS_ALL_APP]:
                # 指定应用
                # 当期下载用户数， 下载用户数全年累计
                current_num = u'当期下载用户数'
                year_num = u'下载用户数全年累计'

                conditions[current_num] = True
                conditions[year_num] = True
                _get_account_active_num(min_region, time_duration, result, conditions, current_num)
                _get_account_active_num(min_region, time_duration_year, result, conditions, year_num)
                pass
            else:
                # 全部应用
                duration_day = (
                    datetime.strptime(time_duration[1], TIME_FORMAT) -
                    datetime.strptime(time_duration[0], TIME_FORMAT)
                ).days
                for key in DOWNLOAD_USER_REPORT_KEYS:
                    conditions[key] = True

                # 厅店日均活跃度
                _get_hall_active_num_daily(min_region, time_duration, result, conditions, KEY_REPORT_HALL_ACTIVE_NUM_DAILY, duration_day)

                # 帐号日均活跃度
                _get_account_active_num_daily(min_region, time_duration, result, conditions, KEY_REPORT_USER_ACTIVE_NUM_DAILY, duration_day)

                # 当期厅店版下载用户数
                sub_conditions = conditions.copy()
                sub_conditions[KEY_CHANNEL_HALL] = True
                sub_conditions[KEY_CHANNEL_HALL_PUSH] = True
                _get_downloadinfo_user_num(min_region, time_duration, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_HALL_CURRENT)
                # 厅店版下载用户数全年累计
                _get_downloadinfo_user_num(min_region, time_duration_year, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_HALL_TOTAL)

                # 当期手机版下载用户数
                sub_conditions = conditions.copy()
                sub_conditions[KEY_CHANNEL_CELL] = True
                _get_downloadinfo_user_num(min_region, time_duration, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_CELL_CURRENT)
                # 手机版下载用户数全年累计
                _get_downloadinfo_user_num(min_region, time_duration_year, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_CELL_TOTAL)

                # 当期下载用户总数
                sub_conditions = conditions.copy()
                for k in CHANEL_KEYS:
                    sub_conditions[k] = True
                _get_downloadinfo_user_num(min_region, time_duration, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_CURRENT)
                # 全年累计下载用户总数
                _get_downloadinfo_user_num(min_region, time_duration_year, result, sub_conditions, KEY_REPORT_DOWNLOAD_USER_NUM_TOTAL)

            response['table_data'] = result

        return HttpResponseJson(response)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])

def app_download_count_view(request):
    """
    渠道业务量统计报表->下载量报表

    全部应用
    1. 报表时间
    2. 区域名称
    3. 厅店日均活跃度
    4. 帐号日均活跃度
    5. 当期厅店版下载量
    6. 当期手机版下载量
    7. 当期仓库版下载量
    8. 当期下载量总数
    9. 厅店版下载量全年累计
    10. 手机版下载量全年累计
    11. 仓库版下载量全年累计
    12. 全年累计下载量总数

    指定应用
    1. 报表时间
    2. 区域名称
    3. 当期下载量
    4. 下载量全年累计
    """
    if request.method == 'GET':
        form_action = reverse('report.views.app_download_count_view')
        return render_to_response('report/app.download.count.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}
            min_region = _get_min_region(conditions)
            time_duration = [conditions['startDate'], conditions['endDate']]
            time_duration_year = [START_OF_YEAR, conditions['endDate']]

            region = _get_region_model(min_region)
            sub_type = _get_sub_type(region)
            sub_regions = _get_sub(region)
            if conditions[KEY_IS_ALL_APP]:
                # 指定应用
                # 当期下载量
                downloads = _get_download_by_region(region, time_duration, conditions)

                res = {}
                for sub_region in sub_regions:
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_CURRENT] = res

                # 下载量全年累计
                downloads = _get_download_by_region(region, time_duration_year, conditions)
                res = {}
                for sub_region in sub_regions:
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_TOTAL] = res
            else:
                # 全部应用
                duration_day = (
                    datetime.strptime(time_duration[1], TIME_FORMAT) -
                    datetime.strptime(time_duration[0], TIME_FORMAT)
                ).days
                for key in DOWNLOAD_APP_REPORT_KEYS:
                    conditions[key] = True

                all_downloads = _get_download_by_region(region, time_duration, conditions)
                # 厅店日均活跃度
                _get_hall_active_num_daily(min_region, time_duration, result, conditions, KEY_REPORT_HALL_ACTIVE_NUM_DAILY, duration_day)
                # 帐号日均活跃度
                _get_account_active_num_daily(min_region, time_duration, result, conditions, KEY_REPORT_USER_ACTIVE_NUM_DAILY, duration_day)
                # 当期厅店版下载量
                downloads = all_downloads.filter(Q(source=2) | Q(source=4))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_HALL_CURRENT] = res

                # 当期手机版下载量
                downloads = all_downloads.filter(Q(source=3))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_CELL_CURRENT] = res

                # 当期仓库版下载量
                downloads = all_downloads.filter(Q(source=1))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_CURRENT] = res
                # 当期下载量总数
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(all_downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_ALL_CURRENT] = res

                all_downloads = _get_download_by_region(region, time_duration_year, conditions)
                # 厅店版下载量全年累计
                downloads = all_downloads.filter(Q(source=2) | Q(source=4))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_HALL_YEAR] = res
                # 手机版下载量全年累计
                downloads = all_downloads.filter(Q(source=3))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_CELL_YEAR] = res
                # 仓库版下载量全年累计
                downloads = all_downloads.filter(Q(source=1))
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_WAREHOUSE_YEAR] = res
                # 全年累计下载量总数
                res = {}
                for sub_region in _get_sub(region):
                    res[sub_region.name] = len(_my_downloads_filter(all_downloads, sub_type, sub_region))
                result[KEY_REPORT_APP_DOWNLOAD_NUM_ALL_YEAR] = res
                pass

            response['table_data'] = result

        return HttpResponseJson(response)

    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def app_download_traffic_view(request):
    """
    渠道业务量统计报表->下载流量报表
    """
    if request.method == 'GET':
        form_action = reverse('report.views.app_download_traffic_view')
        return render_to_response('report/app.download.traffic.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}

            min_region = _get_min_region(conditions)
            time_duration = [conditions['startDate'], conditions['endDate']]
            time_duration_year = [START_OF_YEAR, conditions['endDate']]

            region = _get_region_model(min_region)
            sub_type = _get_sub_type(region)

            all_downloads = _get_download_by_region(region, time_duration, conditions)
            sub_regions = _get_sub(region)
            # 当期手机版产生流量
            _get_traffic(all_downloads.filter(source=3),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_CELL_CURRENT)

            # 当期厅店PUSH产生流量
            _get_traffic(all_downloads.filter(source=4),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_HALL_PUSH_CURRENT)

            # 当期厅店版产生流量
            _get_traffic(all_downloads.filter(Q(source=1)|Q(source=2)),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_HALL_CURRENT)

            # 当期产生流量总计
            _get_traffic(all_downloads,
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_ALL_CURRENT)

            # 当期户均流量
            sub_type = _get_sub_type(region)
            res = dict()
            for sub_region in sub_regions:
                user_count = UserInfo.objects.filter(**{sub_type: sub_region}).count()
                try:
                    res[sub_region.name] = float_format(float(result[KEY_REPORT_TRAFFIC_ALL_CURRENT][sub_region.name]) / user_count)
                except ZeroDivisionError:
                    res[sub_region.name] = 'NAN'
            result[KEY_REPORT_TRAFFIC_ALL_AVG_CURRENT] = res

            all_downloads = _get_download_by_region(region, time_duration_year, conditions)
            sub_regions = _get_sub(region)

            # 手机版产生流量全年累计
            _get_traffic(all_downloads.filter(source=3),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_CELL_YEAR)

            # 厅店PUSH产生流量全年累计
            _get_traffic(all_downloads.filter(source=4),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_HALL_PUSH_YEAR)

            # 厅店版产生流量全年累计
            _get_traffic(all_downloads.filter(Q(source=1) | Q(source=2)),
                         region,
                         sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_HALL_YEAR)

            # 全年累计产生流量
            _get_traffic(all_downloads,
                         region, sub_regions,
                         result,
                         KEY_REPORT_TRAFFIC_ALL_YEAR)
            # 全年累计户均流量
            sub_type = _get_sub_type(region)
            res = dict()
            for sub_region in sub_regions:
                user_count = UserInfo.objects.filter(**{sub_type: sub_region}).count()
                try:
                    res[sub_region.name] = float_format(float(result[KEY_REPORT_TRAFFIC_ALL_YEAR][sub_region.name]) / user_count)
                except ZeroDivisionError:
                    res[sub_region.name] = 'NAN'
            result[KEY_REPORT_TRAFFIC_ALL_AVG_YEAR] = res
            response['table_data'] = result

        return HttpResponseJson(response)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


def app_download_ratio_view(request):
    """
    渠道业务量统计报表->下载流量比报表
    """
    if request.method == 'GET':
        form_action = reverse('report.views.app_download_ratio_view')
        return render_to_response('report/app.download.ratio.inc.html',
                                  locals(),
                                  context_instance=RequestContext(request))
    elif request.method == 'POST':
        response = get_response_code('success')
        conditions = _init_condition_from_request(request)
        if not conditions:
            response = get_response_code('record_not_exist')
        else:
            result = {}
            region = None
            if conditions.get('city'):
                region = City.objects.get(id=conditions['city'])
            else:
                region = Province.objects.get(id=conditions['province'])

            time_duration = [conditions['startDate'], conditions['endDate']]
            time_duration_year = [START_OF_YEAR, conditions['endDate']]

            # 助手辅导用户3G户均流量
            total_traffic = 0
            for d in _get_download_by_region(region, time_duration, conditions, Q(source=2) | Q(source=3) | Q(source=4)):
                total_traffic += d.download_size

            user_count = UserInfo.objects.filter(**{_get_type(region): region}).count()
            if user_count:
                result[KEY_REPORT_RATIO_USER_3G_AVG] = float_format(float(total_traffic) / user_count)
            else:
                result[KEY_REPORT_RATIO_USER_3G_AVG] = 'NAN'

            # TODO 智能终端用户3G户均流量 excel导入
            # TODO 差值（助手辅导用户3G户均流量-智能终端用户3G户均流量）
            # TODO 差幅（助手辅导用户3G户均流量/智能终端用户3G户均流量）
            response['table_data'] = result

        return HttpResponseJson(response)
    else:
        return HttpResponseNotAllowed(['GET', 'POST'])


@log_time
def _get_traffic(downloads, region, sub_regions, result, key):
    sub_type = _get_sub_type(region)
    res = dict()
    for sub_region in sub_regions:
        sub_downloads = _my_downloads_filter(downloads, sub_type, sub_region)
        traffic = 0
        for d in sub_downloads:
            traffic += d.download_size
        res[sub_region.name] = traffic
    result[key] = res
    # logger.info('%s %s: tm=%s' % (region.name, key, float_format(time() - start)))


def _init_condition_from_request(request):
    conditions = dict()
    conditions['startDate'] = request.POST.get('startDate')
    conditions['endDate'] = request.POST.get('endDate')
    conditions['province'] = request.POST.get('province')
    conditions['city'] = request.POST.get('city')
    conditions['district'] = request.POST.get('district')
    conditions['hall'] = request.POST.get('hall')

    if request.POST.get('appRange') == '1':
        conditions[KEY_IS_ALL_APP] = True
    if request.POST.get('appRange') == '2':
        conditions[KEY_IS_ALL_APP] = False

    if 'selectedTargetData' in request.POST:
        selected_target_data = request.POST.getlist('selectedTargetData')
        logger.info(selected_target_data)
        for data in selected_target_data:
            logger.info('data = %s' % data)
            if data != 'multiselect-all':
                conditions[data] = True

    if 'selectedTargetClientType' in request.POST:
        selected_target_client_type = request.POST.getlist('selectedTargetClientType')
        logger.info(selected_target_client_type)
        for client_type in selected_target_client_type:
            logger.info('data = %s' % data)
            if data != 'multiselect-all':
                conditions[client_type] = True

    # valid check
    if conditions['startDate'] == conditions['endDate'] == '' or conditions['province'] == '':
        return None
    return conditions


#FIXME too slow
def _get_account_active_num_daily(min_region, time_duration, result, conditions, key, duration_day):
    _get_account_active_num(min_region, time_duration, result, conditions, key)
    for region_name in result[key].keys():
        result[key][region_name] = float_format(float(result[key][region_name]) / duration_day)

#FIXME too slow
def _get_hall_active_num_daily(min_region, time_duration, result, conditions, key, day):
    _get_hall_active_num(min_region, time_duration, result, conditions, key)
    for region_name in result[key].keys():
        result[key][region_name] = float_format(float(result[key][region_name]) / day)
